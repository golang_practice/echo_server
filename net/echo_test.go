package net

import (
	"bytes"
	"log"
	"net"
	"reflect"
	"testing"
	"time"
)

var tcp, udp Server

func init() {
	// Start the new server
	tcp, err := NewServer("tcp", "localhost:54321")
	if err != nil {
		log.Println("error starting TCP server")
		return
	}

	udp, err := NewServer("udp", "localhost:6250")
	if err != nil {
		log.Println("error starting UDP server")
		return
	}

	// Run the servers in goroutines to stop blocking
	go func() {
		tcp.Run()
	}()
	go func() {
		udp.Run()
	}()
	// it takes ~ 500 microseconds for both the servers to stand up (on my test machine). Otherwise false negatives
	time.Sleep(500 * time.Microsecond)
}

func TestNetServer_server_types(t *testing.T) {
	checkType := func(t testing.TB, servType string, addr string, expectErr bool) {
		t.Helper()
		serv, err := NewServer(servType, "localhost")
		if (err != nil) != expectErr {
			t.Errorf("failed to create server")
		}
		tcpServType := &TCPServer{}
		udpServType := &UDPServer{}
		if servType == "tcp" {
			if reflect.TypeOf(serv) != reflect.TypeOf(tcpServType) {
				t.Error("Type was not tcp", reflect.TypeOf(serv), reflect.TypeOf(tcpServType))
			}
		} else if servType == "udp" {
			if reflect.TypeOf(serv) != reflect.TypeOf(udpServType) {
				t.Error("type was not udp")
			}
		}
	}
	t.Run("tcp server", func(t *testing.T) {
		checkType(t, "tcp", "localhost", false)
	})
	t.Run("udp server", func(t *testing.T) {
		checkType(t, "udp", "locahost", false)
	})
	t.Run("TCP", func(t *testing.T) {
		checkType(t, "TCP", "localhost", false)
	})
	t.Run("badType", func(t *testing.T) {
		checkType(t, "BadType", "localhost", true)
	})
}

func TestNETServer_Running(t *testing.T) {
	// Simply check that the server is up and can
	// accept connections.
	servers := []struct {
		protocol string
		addr     string
	}{
		{"tcp", "localhost:54321"},
		{"udp", "localhost:6250"},
	}
	for _, serv := range servers {
		conn, err := net.Dial(serv.protocol, serv.addr)
		if err != nil {
			t.Error("could not connect to server: ", err)
		}
		defer conn.Close()
	}
}

func TestNETServer_Request(t *testing.T) {
	servers := []struct {
		protocol string
		addr     string
	}{
		{"tcp", "localhost:54321"},
		{"udp", "localhost:6250"},
	}

	tt := []struct {
		test    string
		payload []byte
		want    []byte
	}{
		{"Sending a simple request returns result", []byte("hello world\n"), []byte("Request received: hello world")},
		{"Sending another simple request works", []byte("goodbye world\n"), []byte("Request received: goodbye world")},
	}

	for _, serv := range servers {
		for _, tc := range tt {
			t.Run(tc.test, func(t *testing.T) {
				conn, err := net.Dial(serv.protocol, serv.addr)
				if err != nil {
					t.Error("could not connect to server: ", err)
				}
				defer conn.Close()

				if _, err := conn.Write(tc.payload); err != nil {
					t.Error("could not write payload to server:", err)
				}

				out := make([]byte, 1024)
				if _, err := conn.Read(out); err == nil {
					if bytes.Compare(out, tc.want) == 0 {
						t.Error("response did match expected output")
					}
				} else {
					t.Error("could not read from connection")
				}
			})
		}
	}
}
