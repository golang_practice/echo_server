package main

import (
	"flag"
	"strconv"

	"gitlab.com/sean__ch0n/echo_server/net"
)

func main() {
	addr := flag.String("addr", "localhost", "bind ip")
	port := flag.Int("port", 5555, "bind port")
	flag.Parse()

	servStr := *addr + ":" + strconv.Itoa(*port)
	tcpServ, err := net.NewServer("tcp", servStr)
	if err != nil {
		panic(err)
	}
	tcpServ.Run()

}
